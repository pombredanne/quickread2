var get_html = function(content_node) {
    var content = content_node.content;
    var html = '<li>' + content['title'];

    if (content_node.children !== null && content_node.children.length > 0) {
        html += '\n';
        html += '<ul>\n';

        for (var i = 0; i < content_node.children.length; i++) {
            html += get_html(content_node.children[i]);
        }

        html += '</ul>\n';
    }
    html += '</li>\n';

    return html;
};

var get_table_row = function(content_node, parent) {
    /*
    <tr data-tt-id="1.1.1" data-tt-parent-id="1.1">
        <td>Node 1.1.1: I am part of the tree too!</td>
        <td>That's it!</td>
    </tr>
    */

    var content = content_node.content;
    var key_url = content_node.key_url;
    var id = content['title'].substr(content['title'].lastIndexOf('_') + 1);
    var textarea_id = 'value_' + id.replace(/\./g, '_')
    var parent_part = '';
    var writeable = content_node.writeable;

    if (writeable == null) {
        writeable = false;
    }
    
    if (parent) {
        parent_part = 'data-tt-parent-id="' + parent + '"';
    }
    
    var html = '\n                            <tr data-tt-id="' + id + '"' + parent_part + '>\n';
    html += '<form>';
    html += '                                <td>' + content['title'] + '</td>\n';
    //html += '                                <td>' + content['title'] + ' (' + content_node.access_right + ')' + '</td>\n';
    html += '                                <td><textarea id="' + textarea_id + '">' + content['text'] + '</textarea></td>\n';
    if (writeable) {
        html += '                                <td><button type="submit" id="save_' + id + '" class="table_save_button btn btn-default" data-url="/#" data-operation="save-data" data-method="post" data-id="' + textarea_id + '" data-key-url="' + key_url + '">Speichern</button></td>\n';
    } else {
        html += '                                <td>keine Schreibrechte</td>\n';
    }
    html += '</form>';
    html += '                            </tr>\n';

    if (content_node.children !== null && content_node.children.length > 0) {
        for (var i = 0; i < content_node.children.length; i++) {
            html += get_table_row(content_node.children[i], id);
        }
    }

    return html;
};

var register_button_handler = function() {
    $('button').click(function(e) {
        e.preventDefault();

        var url = $(this).data('url');
        var operation = $(this).data('operation');
        var method = $(this).data('method');
        var data_id = $(this).data('id');
        var data = null;
        var id = $(this).attr('id');
        var key_url = $(this).data('key-url');

        if (data_id) {
            data = $('#' + data_id).val();
        } else {
            data = $(this).data('data');
        }

        if ('post' === method) {
            $.post(url, { 'operation': operation, 'data': data, 'id': id, 'key_url': key_url })
            .done(function(result) {
                var obj = JSON.parse(result);
                $('#internal_content').html('<p>the server returned the success value ' + obj.success + ' at ' + new Date() + '</p>');
            });
        } else if ('get' === method) {
            $.getJSON(url + '?operation=' + operation, function(result) {
                var html = '<p>the server returned the success value ' + result.success + ' at ' + new Date() + '</p>\n';
                var dataObject = JSON.parse(result.data);
                $.each(dataObject, function(key, value){
                    html += get_html(value);
                });
                $('#internal_content').html(html);
            });
        }
    });
}