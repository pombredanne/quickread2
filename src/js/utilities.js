var createAutoClosingAlert = function(selector, delay) {
    $(selector).removeClass('hide');
    $(selector).delay(delay).fadeOut("slow", function () {
        $(this).addClass('hide');
        $(this).show();
    });
};

var doAction = function(e, $actor, callback) {
    e.preventDefault();

    var url = $actor.data('url');
    if (url == null) {
        url = '/action';
    }

    var operation = $actor.data('operation');

    var method = $actor.data('method');
    if (method == null) {
        method = 'post';
    }

    var data;

    if ($actor.is('form')) {
        data = $actor.serialize();
    } else {
        data = JSON.stringify($actor.data());
    }

    if ('post' === method) {
        $.post(url, { 'operation': operation, 'data': data })
        .done(function(result) {
            var obj = JSON.parse(result);
            $('#internal_content').html('<p>the server returned the success value ' + obj.success + ' at ' + new Date() + '</p>');

            if (callback) {
                callback(obj, e);
            }
        });
    } else if ('get' === method) {
        $.getJSON(url + '?operation=' + operation + '&data=' + data, function(result) {
            var html = '<p>the server returned the success value ' + result.success + ' at ' + new Date() + '</p>\n';
            var dataObject = JSON.parse(result.data);
            $.each(dataObject, function(key, value){
                html += get_html(value);
            });
            $('#internal_content').html(html);
        });
    }
};