// "selected_index" is a global variable from index.html

var colorize = function() {
    $visible_rows = $('#tree-content tr:not([style*="display: none;"])');
    var len = $visible_rows.length;
    
    for (var i = 0; i < len; i++) {
        $row = $($visible_rows[i]);
        
        if (i % 2 === 0) {
            $row.removeClass('treetable_dark');
            $row.addClass('treetable_light');
        } else {
            $row.removeClass('treetable_light');
            $row.addClass('treetable_dark');
        }
    };
};

var get_table_row = function(content_node, parent) {
    var content = content_node.content;
    var key_url = content_node.key_url;
    var parent_part = '';
    var text = content.text;
    var title = content.title;
    var user_private = content_node.user_private
    var cellClass = '';
    var writeable = content_node.writeable;

    if (user_private == null) {
        user_private = false;
    }

    if (user_private) {
        cellClass += ' private-node ';
    }
    
    if (parent) {
        parent_part = 'data-tt-parent-id="' + parent + '"';
    }

    // for debugging, you also need to activate output of access right in content_node.py
    //title = title + ' (' + content_node.access_right + ')'
    
    var html = '\n                            <tr data-text="' + text + '" data-writeable="' + writeable + '" data-user-private="' + user_private + '" data-tt-id="' + key_url + '"' + parent_part + '>\n';
    html += '                                <td style="padding-left: 0;" class="' + cellClass + '"><span class="title">' + title + '</span></td>\n';
    html += '                            </tr>\n';

    if (content_node.children !== null && content_node.children.length > 0) {
        for (var i = 0; i < content_node.children.length; i++) {
            html += get_table_row(content_node.children[i], key_url);
        }
    }

    return html;
};

var row_clicked = function(e) {
    var treecontainerOffset = 5 + 20; // from style of #treecontainer and .container.split-pane-frame
    var indenterWidth = $(this).find('.indenter')[0].clientWidth;
    var x = e.clientX;

    if (x > treecontainerOffset + indenterWidth) {
        if (selected_index !== null) {
            $('tr[data-tt-id="' + selected_index + '"]').removeClass('treetable_selected');
        }
        selected_index = $(this).data('tt-id');
        $(this).addClass('treetable_selected');

        $('#nodeTitle').val($(this).find('.title').text());
        $('#nodeText').val($(this).data('text'));
        $('#nodeKeyUrl').val($(this).data('tt-id'));

        // enable edit panel
        $('#nodeText').removeAttr('disabled');
        $('#nodeTitle').removeAttr('disabled');
        $('#editSubmitButton').removeAttr('disabled');

        setToolbarState($(this));
    }
};