// "selected_index" is a global variable from index.html

var setToolbarState = function($table_row) {
    var writeable = $table_row.data('writeable');
    var user_private = $table_row.data('user-private');
    
    if (writeable) {
        $('button[data-operation="add-node"]').removeAttr('disabled');

        if (user_private) {
            $('button[data-operation="delete-node"]').attr('disabled', 'disabled');
        } else {
            $('button[data-operation="delete-node"]').removeAttr('disabled');
        }
    } else {
        $('button[data-operation="add-node"]').attr('disabled', 'disabled');
        $('button[data-operation="delete-node"]').attr('disabled', 'disabled');
    }
};

var addnodeSuccess = function(obj) {
    content_node = JSON.parse(obj.data);
    
    var table_row = get_table_row(content_node, selected_index);
    var id = $(table_row).data('tt-id');
    
    $("#treetable").treetable('loadBranch', $("#treetable").treetable('node', selected_index), table_row);
    $("#treetable").treetable('expandNode', selected_index);
    $('tr[data-tt-id="' + id + '"]').click(row_clicked);
};

var deletenodeSuccess = function() {
    // disable edit panel
    $('#nodeText').attr('disabled', 'disabled');
    $('#nodeText').val('');
    $('#nodeTitle').attr('disabled', 'disabled');
    $('#nodeTitle').val('');
    $('#editSubmitButton').attr('disabled', 'disabled');
    // remove node from tree
    $("#treetable").treetable('removeNode', selected_index);
    selected_index = null;
};

var setToolbarHandler = function() {
    $('.btn-toolbar').click(function(e) {
        var $button = $(this);
        var $selected_row = $('tr[data-tt-id="' + selected_index + '"]')
        var operation = $button.data('operation');
        $selected_row.data('operation', operation)
        $selected_row.data('keyUrl', $selected_row.data('tt-id'))

        doAction(e, $selected_row, function(obj, e) {
            if (obj.success) {
                window[operation.replace('-','') + 'Success'](obj);
                $.pnotify({
                    title: 'Erfolg!',
                    text: $button.attr('text-success'),
                    type: 'success',
                    width: '100%',
                    addclass: 'stack-bar-bottom',
                    stack: stack_bar_bottom
                });
            } else {
                $.pnotify({
                    title: 'Fehler!',
                    text: $button.attr('text-fail'),
                    type: 'error',
                    width: '100%',
                    addclass: 'stack-bar-bottom',
                    stack: stack_bar_bottom
                });
            }
        });

        $button.blur();
    });
};