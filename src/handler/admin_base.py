import jinja_worker, json
import os, sys

from google.appengine.api import users

sys.path.append(os.path.join(os.path.dirname(__file__), '../model'))

from jinja2 import Template
from google.appengine.ext import ndb
from content_node import ContentNode
from acl_node import ACLNode

import create_demo_data, list_demo_data

def get_bool(text):
    return (False != text) and (text.lower() in ['', 'true'])

class AdminBaseHandler(jinja_worker.Handler_jinja_worker):
    
    def get(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)
            
            if operation == 'list-demo-data':
                data = list_demo_data.execute()
                if data:
                    result_dict = { 'success': True, 'data': data }
                else:
                    result_dict = { 'success': False }
                
                self.response.headers.add_header('Access-Control-Allow-Origin', '*')
                self.response.write(json.dumps(result_dict))
            else:
                username = user.nickname()
                url = users.create_logout_url('/')
                navigation_top = self.render_str(template = 'navigation.top', username = user.nickname(), url = users.create_logout_url('/'), is_admin = users.is_current_user_admin())

                footer = self.render_str(template = 'footer')
                self.set_autoescape(False)
                site = self.render_str(template = 'admin', username = username, url = url, navigation_top = navigation_top, footer = footer)
                self.set_autoescape(True)
                self.write(site)
        else:
            username = None
            url = users.create_login_url('/')
            navigation_top = self.render_str(template = 'navigation.top', username = None, url = users.create_login_url('/'), is_admin = False)
        
            footer = self.render_str(template = 'footer')
            self.set_autoescape(False)
            site = self.render_str(template = 'admin', username = username, url = url, navigation_top = navigation_top, footer = footer)
            self.set_autoescape(True)
            self.write(site)
    
    def post(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)
            
            if operation == 'create-demo-data':
                if create_demo_data.execute():
                    result_dict = { 'success': True }
                else:
                    result_dict = { 'success': False }
                
                self.response.headers.add_header('Access-Control-Allow-Origin', '*')
                self.response.write(json.dumps(result_dict))
            elif operation == 'delete-demo-data':
                ndb.delete_multi(ContentNode.query().fetch(keys_only=True))
                ndb.delete_multi(ACLNode.query().fetch(keys_only=True))
    
    def test(self):
        pass