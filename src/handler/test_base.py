import pprint
pp = pprint.PrettyPrinter(indent=4)

import jinja_worker
import os, sys, json

from google.appengine.api import users
from google.appengine.ext import ndb

sys.path.append(os.path.join(os.path.dirname(__file__), '../model'))

from jinja2 import Template

def get_bool(text):
    return (False != text) and (text.lower() in ['', 'true'])

class TestBaseHandler(jinja_worker.Handler_jinja_worker):
    
    withhtml = True
    withdata = False
    
    def get(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)

            if operation == 'list-data':
                self.__webservice__list_data()
            else:
                self._get_home_page(user)
        else:
            self._get_no_user()

    def post(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)
            
            if operation == 'save-data':
                self.__webservice__save_data(user)
            elif operation == 'delete-demo-data':
                pass
    
    def extract_parameter(self):
        for item in self.request.GET.items():
            if 'with-html' == item[0]:
                self.withhtml = get_bool(item[1])
            
            if 'without-html' == item[0]:
                self.withhtml = not get_bool(item[1])
            
            if 'with-data' == item[0]:
                self.withdata = get_bool(item[1])
            
            if 'without-data' == item[0]:
                self.withdata = not get_bool(item[1])
    
    def _get_home_page(self, user):
        base_template = self.request.environ['PATH_INFO'][1:-5]
        self.extract_parameter()
        username = user.nickname()
        url = users.create_logout_url('/')
        navigation_top = self.render_str(template = 'navigation.top', username = user.nickname(), url = users.create_logout_url('/'), is_admin = users.is_current_user_admin())
        footer = self.render_str(template = 'footer')
        self.set_autoescape(False)
        site = self.render_str(template = base_template, username = username, url = url, navigation_top = navigation_top, footer = footer)
        self.set_autoescape(True)
        self.response.write(site)
    
    def _get_no_user(self):
        username = None
        url = users.create_login_url('/')
        navigation_top = self.render_str(template = 'navigation.top', username = None, url = users.create_login_url('/'), is_admin = False)
        footer = self.render_str(template = 'footer')
        self.set_autoescape(False)
        site = self.render_str(template = 'base', username = username, url = url, navigation_top = navigation_top, footer = footer)
        self.set_autoescape(True)
        self.response.write(site)
    
    def __webservice__list_data(self):
        import list_data
        data = list_data.execute()
        if data:
            result_dict = { 'success': True, 'data': data }
        else:
            result_dict = { 'success': False }

        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __webservice__save_data(self, user):
        from acl_node import ACLNode

        content_node = None
        key_url = self.request.get('key_url', default_value = None)
        if key_url:
            key = ndb.Key(urlsafe = key_url)
            content_node = key.get()

        if content_node:
            data = self.request.get('data', default_value = None)
            acl_node = ACLNode.get(user.user_id(), content_node.key)

            if acl_node and acl_node.has_write_right():
                content_object = json.loads(content_node.content)
                content_object['text'] = data
                content_node.content = json.dumps(content_object)
                content_node.put()
                result_dict = { 'success': True, 'data': content_node.to_json(user) }
            else:
                result_dict = { 'success': False, 'error': 'no write access' }
        else:
            result_dict = { 'success': False, 'error': 'no content node found' }
        
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))