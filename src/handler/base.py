import pprint
pp = pprint.PrettyPrinter(indent=4)

import jinja_worker
import os, sys, json, urlparse

from google.appengine.api import users
from google.appengine.ext import ndb

sys.path.append(os.path.join(os.path.dirname(__file__), '../model'))

from jinja2 import Template
from acl_node import ACLNode
from content_node import ContentNode

import list_data

import time

current_time_milli_str = lambda: str(int(round(time.time() * 1000)))

def get_bool(text):
    return (False != text) and (text.lower() in ['', 'true'])

class BaseHandler(jinja_worker.Handler_jinja_worker):
    
    withhtml = True
    withdata = False
    
    def get(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)

            if operation == 'list-data':
                self.__webservice__list_data()
            else:
                self._get_home_page(user)
        else:
            self._get_no_user()

    def post(self):
        user = users.get_current_user()
        
        if user:
            operation = self.request.get('operation', default_value = None)
            
            if operation == 'save-data':
                self.__webservice__save_data(user)
            elif operation == 'edit-data':
                self.__webservice__edit_data(user)
            elif operation == 'add-node':
                self.__webservice__add_node(user)
            elif operation == 'delete-node':
                self.__webservice__delete_node(user)
    
    def extract_parameter(self):
        for item in self.request.GET.items():
            if 'with-html' == item[0]:
                self.withhtml = get_bool(item[1])
            
            if 'without-html' == item[0]:
                self.withhtml = not get_bool(item[1])
            
            if 'with-data' == item[0]:
                self.withdata = get_bool(item[1])
            
            if 'without-data' == item[0]:
                self.withdata = not get_bool(item[1])
    
    def _get_home_page(self, user):
        self.extract_parameter()
        username = user.nickname()
        url = users.create_logout_url('/')
        navigation_top = self.render_str(template = 'navigation.top', username = user.nickname(), url = users.create_logout_url('/'), is_admin = users.is_current_user_admin())
        toolbar = self.render_str(template = 'toolbar')
        footer = self.render_str(template = 'footer')
        self.set_autoescape(False)
        site = self.render_str(template = 'index', username = username, url = url, navigation_top = navigation_top, footer = footer, toolbar = toolbar)
        self.set_autoescape(True)
        self.response.write(site)
    
    def _get_no_user(self):
        username = None
        url = users.create_login_url('/')
        navigation_top = self.render_str(template = 'navigation.top', username = None, url = users.create_login_url('/'), is_admin = False)
        footer = self.render_str(template = 'footer')
        self.set_autoescape(False)
        site = self.render_str(template = 'base', username = username, url = url, navigation_top = navigation_top, footer = footer)
        self.set_autoescape(True)
        self.response.write(site)
    
    def __webservice__list_data(self):
        data = list_data.execute()
        if data:
            result_dict = { 'success': True, 'data': data }
        else:
            result_dict = { 'success': False }

        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __webservice__save_data(self, user):
        content_node = None
        key_url = self.request.get('key_url', default_value = None)
        if key_url:
            key = ndb.Key(urlsafe = key_url)
            content_node = key.get()
        else:
            result_dict = { 'success': False, 'error': 'no key given' }

        if content_node:
            data = self.request.get('data', default_value = None)
            acl_node = ACLNode.get(user.user_id(), content_node.key)

            if acl_node and acl_node.has_write_right():
                content_object = json.loads(content_node.content)
                content_object['text'] = data
                content_node.content = json.dumps(content_object)
                content_node.put()
                result_dict = { 'success': True, 'data': content_node.to_json(user) }
            else:
                result_dict = { 'success': False, 'error': 'no write access' }
        else:
            result_dict = { 'success': False, 'error': 'no content node found' }
        
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __webservice__edit_data(self, user):
        content_node = None
        raw_data = self.request.get('data', default_value = None)
        data = urlparse.parse_qs(raw_data)

        if data:
            key_url = data['nodeKeyUrl'][0].encode('utf8','ignore')

            if key_url:
                key = ndb.Key(urlsafe = key_url)
                content_node = key.get()
            else:
                result_dict = { 'success': False, 'error': 'no key given' }

            if content_node:
                acl_node = ACLNode.get(user.user_id(), key)

                if acl_node and acl_node.has_write_right():
                    # I'm not sure what I'm doing here :-/ damned encoding ...

                    #title = data['nodeTitle'][0].encode('utf8','ignore')
                    if data.has_key('nodeTitle'):
                        title = data['nodeTitle'][0].encode('iso-8859-1','ignore')
                    else:
                        title = ''

                    #text = data['nodeText'][0].encode('utf8','ignore')
                    if data.has_key('nodeText'):
                        text = data['nodeText'][0].encode('iso-8859-1','ignore')
                    else:
                        text = ''

                    content_object = json.loads(content_node.content)
                    content_object['title'] = title
                    content_object['text'] = text
                    content_node.content = json.dumps(content_object)
                    content_node.put()
                    result_dict = { 'success': True, 'data': content_node.to_json(user) }
                else:
                    result_dict = { 'success': False, 'error': 'no write access' }
            else:
                result_dict = { 'success': False, 'error': 'no content node found' }
        else:
            result_dict = { 'success': False, 'error': 'no data given' }
        
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __webservice__add_node(self, user):
        parent_content_node = None
        raw_data = self.request.get('data', default_value = None)
        data = json.loads(raw_data)

        if data:
            parent_key_url = data['keyUrl']

            if parent_key_url:
                parent_key = ndb.Key(urlsafe = parent_key_url)
                parent_content_node = parent_key.get()
            else:
                result_dict = { 'success': False, 'error': 'no parent key given' }

            if parent_content_node:
                parent_acl_node = ACLNode.get(user.user_id(), parent_key)

                if parent_acl_node and parent_acl_node.has_write_right():
                    # create new content node
                    new_content_node = ContentNode(content = '{ "title": "new node ' + current_time_milli_str() + '", "text": "no text" }', parent = parent_key)
                    new_content_node_key = new_content_node.put()
                    
                    # add new node to parents children
                    parent_content_node.children.append(new_content_node_key)
                    parent_content_node.put()

                    # create new acl for new content node
                    new_acl_node = ACLNode(user_id = user.user_id(), content_node_key = new_content_node_key, access_right = 7)
                    new_acl_node.put()

                    # return success and the complete new content node
                    result_dict = { 'success': True, 'data': new_content_node.to_json(user) }
                else:
                    result_dict = { 'success': False, 'error': 'no write access' }
            else:
                result_dict = { 'success': False, 'error': 'no content node found' }
        else:
            result_dict = { 'success': False, 'error': 'no data given' }
        
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __webservice__delete_node(self, user):
        parent_content_node = None
        raw_data = self.request.get('data', default_value = None)
        data = json.loads(raw_data)

        if data:
            key_url = data['keyUrl']

            if key_url:
                key = ndb.Key(urlsafe = key_url)
                content_node = key.get()

                if content_node.is_users_private_node(user):
                    result_dict = { 'success': False, 'error': 'cannot delete private root node' }
                else:
                    parent_key = key.parent()
                    parent_content_node = parent_key.get()
                    parent_content_node.children.remove(key)
                    parent_content_node.put()
                    self.__deep_delete_node(key)

                    result_dict = { 'success': True }
            else:
                result_dict = { 'success': False, 'error': 'no key given' }
        else:
            result_dict = { 'success': False, 'error': 'no data given' }
        
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        self.response.write(json.dumps(result_dict))

    def __deep_delete_node(self, key):
        content_node = key.get()

        if len(content_node.children) > 0:
            for child in content_node.children:
                self.__deep_delete_node(child)

        acl_nodes = ACLNode.get_by_content_node(key)
        for acl_node in acl_nodes:
            acl_node.key.delete()
        key.delete()