import os, sys
import webapp2

sys.path.append(os.path.join(os.path.dirname(__file__), 'handler'))

import base, admin_base, test_base

app = webapp2.WSGIApplication([
    ('/admin', admin_base.AdminBaseHandler),
    ('/test/.*', test_base.TestBaseHandler),
    ('/', base.BaseHandler),
    ('.*', base.BaseHandler)
], debug=True)
