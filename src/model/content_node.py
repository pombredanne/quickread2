from google.appengine.ext import ndb
import json
from acl_node import ACLNode

def bool2str(bool):
    if bool:
        return 'true'
    else:
        return 'false'

class ContentNode(ndb.Model):
    content = ndb.JsonProperty()
    children = ndb.KeyProperty(kind = 'ContentNode', repeated = True)
    cached_private_root_content_parent_key = None
    
    def get_content_as_dict(self):
        return json.loads(self.content)
    
    def print_me(self):
        print self.content
        for child in self.children:
            child.get().print_me()

    def is_users_private_node(self, user):
        if not self.cached_private_root_content_parent_key:
            self.cached_private_root_content_parent_key = ndb.Key(ContentNode, str(user.user_id()))
        return self.cached_private_root_content_parent_key == self.key.parent()
    
    def to_json(self, user = None):
        if user:
            acl_node = ACLNode.get(user.user_id(), self.key)

        text = '{'
        text += '"content": ' + self.content + ','

        if user:
            text += '"user_private": ' + bool2str(self.is_users_private_node(user)) + ','

            if acl_node:
                text += '"readable": ' + bool2str(acl_node.has_read_right()) + ','
                text += '"writeable": ' + bool2str(acl_node.has_write_right()) + ','
                #text += '"access_right": ' + str(acl_node.access_right) + ','

        text += '"key_url": "' + self.key.urlsafe() + '",'
        text += '"children": ['
        
        if self.children:
            for child in self.children:
                text += child.get().to_json(user) + ', '
            text = text[:-2]
        
        text += ']'
        text += '}'
        
        return text