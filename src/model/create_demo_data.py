#! /usr/bin/python

import random
from content_node import ContentNode
from acl_node import ACLNode
from google.appengine.ext import ndb

test_user_id = '185804764220139124118' # bzw. gae testuser

def _create_node(parent_number = '', parent_key = None):
    content_node = ContentNode(content = '{ "title": "content_node_' + parent_number + '", "text": "some text" }', parent = parent_key)
    key = content_node.put()
    
    if random.random() < 0.7:
        content_node.children = []
        for i in range(1, random.randint(1, 3)):
            child_key = _create_node(parent_number + "." + str(i), key)
            content_node.children.append(child_key)
    
    key = content_node.put()
    
    return key

def _create_content():
    root_key = ndb.Key(ContentNode, 'root_content_node')
    root_node = ContentNode(content = '{ "title": "root_content_node", "text": "some text" }', key = root_key)
    root_node.put()
    
    for i in range(1, random.randint(3, 7)): # 3 to 7 nodes in total
        content_node_key = _create_node(str(i), root_key)
        root_node.children.append(content_node_key)
    
    root_node.put()
    
    return True

def _create_random_right_on_node_recursively(content_node_key):
    acl_node = ACLNode.get(test_user_id, content_node_key)
    if not acl_node:
        acl_node = ACLNode(user_id = test_user_id, content_node_key = content_node_key, access_right = random.randint(0, 7))
        acl_node.put()
        for child in content_node_key.get().children:
            _create_random_right_on_node_recursively(child)

def _create_access_rights():
    root_content_node = ndb.Key(ContentNode, 'root_content_node').get()
    number = random.randint(0, len(root_content_node.children) - 1)
    
    for i in range(0, number):
        index = random.randint(0, len(root_content_node.children) - 1)
        content_node_key = root_content_node.children[index]
        _create_random_right_on_node_recursively(content_node_key)
    
    return True
    
def execute():
    _create_content()
    _create_access_rights()
    
    return True

if __name__ == "__main__":
    execute()
    
    
