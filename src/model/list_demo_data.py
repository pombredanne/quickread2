#! /usr/bin/python

from content_node import ContentNode
from google.appengine.ext import ndb

def execute():
    root_key = ndb.Key(ContentNode, 'root_content_node')
    root_node = root_key.get()
    result = '['

    if root_node:
        content_node_key_list = root_node.children
        
        if content_node_key_list:
            for content_node_key in content_node_key_list:
                result += content_node_key.get().to_json() + ', '
            result = result[:-2]
    
    result += ']'
    
    return result
    
if __name__ == "__main__":
    execute()
    
    
