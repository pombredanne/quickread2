from google.appengine.ext import ndb

class ACLNode(ndb.Model):
    user_id = ndb.StringProperty(indexed = True, required = True)
    content_node_key = ndb.KeyProperty(indexed = True, required = True)
    # linux file system like access right (three lowest bits, LSB right)
    # a bit set to true means the permission is granted
    # bit 0: execute permission (not used)
    # bit 1: write permission
    # bit 2: read permission
    access_right = ndb.IntegerProperty(required = True)

    def has_read_right(self):
        return self.access_right & 4 != 0

    def has_write_right(self):
        return self.access_right & 2 != 0

    def has_execute_right(self):
        return self.access_right & 1 != 0
    
    @staticmethod
    def get_by_user(user_id):
        query = ACLNode.query(ACLNode.user_id == user_id)
        return query.fetch()
    
    @staticmethod
    def get_by_content_node(content_node_key):
        query = ACLNode.query(ACLNode.content_node_key == content_node_key)
        return query.fetch()
    
    @staticmethod
    def get(user_id, content_node_key):
        result = None
        result_list = ACLNode.query(ACLNode.user_id == user_id, ACLNode.content_node_key == content_node_key).fetch()

        if len(result_list) > 0:
            result = result_list[0]
            if len(result_list) > 1:
                print 'ERROR: multiple ACL for one user on one content node found (db inconsistency, only one possible): user id = "' + str(user_id) + '", content node key = "' + str(content_node_key) + '"'
                pass
        
        return result
    
    @staticmethod
    def is_readable(user_id, content_node_key):
        return ACLNode.get(user_id, content_node_key).has_read_right()
    
    @staticmethod
    def is_writeable(user_id, content_node_key):
        return ACLNode.get(user_id, content_node_key).has_write_right()