#! /usr/bin/python

from google.appengine.api import users
from google.appengine.ext import ndb
from acl_node import ACLNode
from content_node import ContentNode
import json

def get_ancestor_depth(key):
    if key == None:
        return 0
    else:
        return get_ancestor_depth(key.parent()) + 1

def is_ancestor_of(possible_ancestor, key):
    if not possible_ancestor:
        return False
    else:
        possible_ancestor_depth = get_ancestor_depth(possible_ancestor)
        key_depth = get_ancestor_depth(key)
        
        if possible_ancestor_depth > key_depth:
            return False
        else:
            if key == possible_ancestor:
                return True
            else:
                return is_ancestor_of(possible_ancestor, key.parent())

def execute():
    user = users.get_current_user()

    # check if user has a private node, create one if not
    private_root_content_parent_key = ndb.Key(ContentNode, str(user.user_id()))
    query = ContentNode.query(ancestor = private_root_content_parent_key) # this gets all nodes under the private node, TODO: this must be done better
    temp_list = query.fetch()
    result_list = []

    # filter out only the direct children of the private (root) node
    for content_node in temp_list:
        if content_node.key.parent() == private_root_content_parent_key:
            result_list.append(content_node)

    new_private_acl_node = None
    if len(result_list) == 0:
        new_private_content_node = ContentNode(content = '{ "title": "private node of ' + user.email() + '", "text": "no text" }', parent = private_root_content_parent_key)
        private_root_content_node_key = new_private_content_node.put()
        new_private_acl_node = ACLNode(user_id = user.user_id(), content_node_key = private_root_content_node_key, access_right = 7)
        private_root_acl_node_key = new_private_acl_node.put()
    elif len(result_list) == 1:
        private_root_content_node_key = result_list[0].key
        private_root_acl_node = ACLNode.get(user.user_id(), private_root_content_node_key)
        if not private_root_acl_node:
            ACLNode(user_id = user.user_id(), content_node_key = private_root_content_node_key, access_right = 7).put()
        else:
            if not private_root_acl_node.access_right == 7:
                private_root_acl_node.access_right = 7
                private_root_acl_node.put()
    else:
        print 'ERROR: multiple private content nodes found for user ' + str(user.user_id())
        for key in result_list:
            print '     ' + str(key)
        pass

    # get all acl nodes with read access
    acl_nodes = ACLNode.get_by_user(user.user_id())

    # it seems to me that adding a new private node (content and acl) to the datastore takes some time
    # in the background such that it may be the case that a query does not find the fresh acl node,
    # so I manually add the new acl node the the list of this user's acl nodes
    # the content node however can be found as it is accessed by it's key
    if new_private_acl_node and not new_private_acl_node in acl_nodes:
        acl_nodes.append(new_private_acl_node)

    content_node_key_list = []
    node_iterator = iter(obj for obj in acl_nodes if obj.has_read_right())
    for acl_node in node_iterator:
        content_node_key_list.append(acl_node.content_node_key)

    # get all (readable) content nodes of the found acl nodes
    # respect the hierarchy: if a node is readable, all nodes of its subtree are readable unless one descendant has it's own acl node!
    temp_keys = list(content_node_key_list)
    for content_node_key in content_node_key_list:
        for possible_ancestor in content_node_key_list:
            if not possible_ancestor == content_node_key and is_ancestor_of(possible_ancestor, content_node_key):
                temp_keys.remove(content_node_key)
                break

    result = '['

    if len(temp_keys) > 0:
        for content_node_key in temp_keys:
            result += content_node_key.get().to_json(user) + ', '
        result = result[:-2]
    
    result += ']'
    
    return result
    
if __name__ == "__main__":
    execute()