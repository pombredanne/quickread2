# TODO: I can't get GAE with NDB run a unit test :-(

import sys, os, time
import unittest

sys.path.append(os.path.join(os.path.dirname(__file__), '../../model'))
#sys.path.append(os.path.join('/usr/local/google_appengine'))
#sys.path.append(os.path.join('/usr/local/google_appengine/lib/yaml-3.10'))

from google.appengine.ext import ndb
from google.appengine.ext import testbed

from acl_node import ACLNode
from content_node import ContentNode

TEST_ROOT_CONTENT_KEY = ndb.Key(ContentNode, 'test.model.Test_ACLNode')

def get_by_user_id(user_text):
    return user_text.split('_')[-1]

def deep_delete_content_node(key):
    entity = key.get()
    if (False != entity and len(entity.children) > 0):
        for child in entity.children:
            deep_delete_content_node(child.key);
    key.delete()

class Test_ACLNode(unittest.TestCase):
    
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
#        self.testbed.init_memcache_stub()
        
        deep_delete_content_node(TEST_ROOT_CONTENT_KEY)
        ContentNode(content = None, key = TEST_ROOT_CONTENT_KEY).put()
        self.cn_1 = ContentNode(content = "{ 'name': 'Content Node 1' }", parent = TEST_ROOT_CONTENT_KEY).put()
        self.cn_2 = ContentNode(content = "{ 'name': 'Content Node 2' }", parent = TEST_ROOT_CONTENT_KEY).put()
        self.cn_3 = ContentNode(content = "{ 'name': 'Content Node 3' }", parent = TEST_ROOT_CONTENT_KEY).put()

    def tearDown(self):
        deep_delete_content_node(TEST_ROOT_CONTENT_KEY)
        
        self.testbed.deactivate()
    
    def test_methods(self):
        # TODO: JSONify this !! Maybe in a seperate file with a nice loader and stuff.
        
        # ######################################################################
        
        # 1. get_by_user
        # 1.1 ACL_1: cn_1, user_1
        # 1.1.1 get_by_user(user_1) = ACL_1
        # 1.1.2 get_by_user(user_2) = None
        # 1.2 ACL_1: cn_1, user_1
        #     ACL_2: cn_1, user_2
        # 1.2.1 get_by_user(user_1) = ACL_1
        # 1.2.2 get_by_user(user_2) = ACL_1
        # 1.2.3 get_by_user(user_3) = None
        # 1.3. ACL_1: cn_1, user_1
        #      ACL_2: cn_2, user_1
        # 1.3.1 get_by_user(user_1) = ACL_1, ACL_2
        
        # 2. get_content_node
        # 2.1 ACL_1: cn_1, user_1
        # 2.1.1 get_content_node(cn_1): ACL_1
        # 2.1.1 get_content_node(cn_2): None
        # 2.2 ACL_1: cn_1, user_1
        #     ACL_2: cn_1, user_2
        #     ACL_3: cn_2, user_3
        # 2.2.1 get_content_node(cn_1): ACL_1, ACL_2
        # 2.2.2 get_content_node(cn_2): ACL_3
        # 2.2.2 get_content_node(cn_3): None
        # 2.3 ACL_1: cn_1, user_1
        #     ACL_2: cn_2, user_1
        # 2.3.1 get_content_node(cn_1): ACL_1
        
        # 3. get_access_right
        # 3.1 ACL_1: cn_1, user_1, access_right = 7
        # 3.1.1 get_access_right(user_1,cn_1): 7
        # 3.1.2 get_access_right(user_1,cn_2): None
        # 3.1.3 get_access_right(user_2,cn_1): None
        # 3.1.4 get_access_right(user_2,cn_2): None
        
        # 4. is_readable
        # 4.1 ACL_1: cn_1, user_1, access_right = 0
        # 4.1.1 is_readable(user_1, cn_1): False
        # 4.1.2 is_readable(user_2, cn_1): False
        # 4.1.3 is_readable(user_1, cn_2): False
        # 4.1.4 is_readable(user_2, cn_2): False
        # 4.2 ACL_1: cn_1, user_1, access_right = 3
        # 4.2.1 is_readable(user_1, cn_1): False
        # 4.3 ACL_1: cn_1, user_1, access_right = 4
        # 4.3.1 is_readable(user_1, cn_1): True
        # 4.3.2 is_readable(user_1, cn_2): False
        # 4.3.3 is_readable(user_2, cn_1): False
        # 4.3.4 is_readable(user_2, cn_2): False
        # 4.4 ACL_1: cn_1, user_1, access_right = 7
        # 4.4.1 is_readable(user_1, cn_1): True
        
        # 5. is_writeable
        # 5.1 ACL_1: cn_1, user_1, access_right = 0
        # 5.1.1 is_readable(user_1, cn_1): False
        # 5.1.2 is_readable(user_2, cn_1): False
        # 5.1.3 is_readable(user_1, cn_2): False
        # 5.1.4 is_readable(user_2, cn_2): False
        # 5.2 ACL_1: cn_1, user_1, access_right = 2
        # 5.2.1 is_readable(user_1, cn_1): True
        # 5.2.2 is_readable(user_2, cn_1): False
        # 5.2.3 is_readable(user_1, cn_2): False
        # 5.2.4 is_readable(user_2, cn_2): False
        # 5.3 ACL_1: cn_1, user_1, access_right = 4
        # 5.3.1 is_readable(user_1, cn_1): False
        # 5.4 ACL_1: cn_1, user_1, access_right = 7
        # 5.4.1 is_readable(user_1, cn_1): True
        
        # ######################################################################

        testblocks = {
            'get_by_user': {
                'Test 1.1': {
                    'acls': [
                        { 'content_node': 'cn_1', 'user': 'user_1' }
                    ],
                    'Test 1.1.1': {
                        'parameter': [ 'user_1' ],
                        'result': [1]
                    },
                    'Test 1.1.2': {
                        'parameter': [ 'user_2' ],
                        'result': []
                    }
                },
                'Test 1.2': {
                    'acls': [
                        { 'content_node': 'cn_1', 'user': 'user_1' },
                        { 'content_node': 'cn_1', 'user': 'user_2' }
                    ],
                    'Test 1.2.1': {
                        'parameter': [ 'user_1' ],
                        'result': [1]
                    },
                    'Test 1.2.2': {
                        'parameter': [ 'user_2' ],
                        'result': [1]
                    },
                    'Test 1.2.3': {
                        'parameter': [ 'user_3' ],
                        'result': []
                    }
                },
                'Test 1.3': {
                    'acls': [
                        { 'content_node': 'cn_1', 'user': 'user_1' },
                        { 'content_node': 'cn_2', 'user': 'user_1' }
                    ],
                    'Test 1.3.1': {
                        'parameter': [ 'user_1' ],
                        'result': [1, 2]
                    }
                }
            }
        }

        for method in testblocks:
            testblock = testblocks[method]
            
            for testcases_key in testblock:
                testcases = testblock[testcases_key]
                
                for testcase in testcases:
                    acls = testcases[testcase]['acls']
                    for acl in acls:
                        aclNode = ACLNode(user_id = get_by_user_id(acl['user']), content_node_key = getattr(self, acl['content_node']))
                        # TODO

if __name__ == '__main__':
    unittest.main()
