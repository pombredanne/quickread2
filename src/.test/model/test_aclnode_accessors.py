import unittest
from google.appengine.ext import ndb

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../../model'))
from acl_node import ACLNode
from content_node import ContentNode

test_user_id = '185804764220139124118' # bzw. gae testuser

class Test_ACLNode_accessors(unittest.TestCase):

    def setUp(self):
        pass

    def test_has_read_right(self):
        acl_node = ACLNode(user_id = test_user_id, access_right = 0)
        assert not acl_node.has_read_right()
        assert not acl_node.has_write_right()
        assert not acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 1)
        assert not acl_node.has_read_right()
        assert not acl_node.has_write_right()
        assert acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 2)
        assert not acl_node.has_read_right()
        assert acl_node.has_write_right()
        assert not acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 3)
        assert not acl_node.has_read_right()
        assert acl_node.has_write_right()
        assert acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 4)
        assert acl_node.has_read_right()
        assert not acl_node.has_write_right()
        assert not acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 5)
        assert acl_node.has_read_right()
        assert not acl_node.has_write_right()
        assert acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 6)
        assert acl_node.has_read_right()
        assert acl_node.has_write_right()
        assert not acl_node.has_execute_right()

        acl_node = ACLNode(user_id = test_user_id, access_right = 7)
        assert acl_node.has_read_right()
        assert acl_node.has_write_right()
        assert acl_node.has_execute_right()

if __name__ == '__main__':
    unittest.main()