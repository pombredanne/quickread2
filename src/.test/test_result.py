class TestResult():
    def __init__(self, title = '', success = True, message = '', parent = None):
        self.title = title
        self.success = success
        self.child_test_results = []
        self.message = message
        
        if parent:
            parent.add_child(self)
    
    def output(self, indent_depth = 0):
        output = ''
        indent = indent_depth * ' '
        success = 'unsuccessful' if not self.success else 'successful'
        msg = ': ' + self.message if not self.success else ''
        
        if len(self.child_test_results) > 0:
            output += '<span class="title">' + self.title + '</span> <span class="' + success + '">' + success + msg + '</span>\n'
            output += indent + '<ul>\n';
            for child_test_result in self.child_test_results:
                output += child_test_result.output(indent_depth + 2)
            output += '</ul>\n';
        else:
            output = indent + '<li><span class="title">' + self.title + '</span> <span class="' + success + '">' + success + msg + '</span></li>\n'

        return output
    
    def add_child(self, child_test_result):
        self.child_test_results.append(child_test_result)
        child_test_result.parent = self