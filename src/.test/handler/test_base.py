import os, sys, time, unittest
from google.appengine.ext import testbed

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../../handler'))

from base import BaseHandler
import webapp2

class Test_BaseHandler(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()

    def tearDown(self):
        self.testbed.deactivate()

    def test_extract_parameter(self):
        # TODO: JSONify this !! Maybe in a seperate file with a nice loader and stuff.
        
        # ######################################################################
        # withhtml
        
        # default True, case insensitive
        # /test                                         withhtml = True
        # /test?withhtml                                withhtml = True
        # /test?withhtml=True                           withhtml = True
        # /test?withhtml=true                           withhtml = True
        # /test?withhtml=trUE                           withhtml = True
        
        # everything else is False
        # /test?withhtml=False                          withhtml = False
        # /test?withhtml=false                          withhtml = False
        # /test?withhtml=FaLSe                          withhtml = False
        # /test?withhtml=nigges                         withhtml = False
        
        # default False, case insensitive
        # /test?without-html                            withhtml = False
        # /test?without-html=True                       withhtml = False
        # /test?without-html=true                       withhtml = False
        # /test?without-html=tRUe                       withhtml = False
        
        # everything else is True
        # /test?without-html=False                      withhtml = True
        # /test?without-html=false                      withhtml = True
        # /test?without-html=faLSE                      withhtml = True
        # /test?without-html=yeah                       withhtml = True
        
        # order matters, last one wins
        # /test?without-html&with-html                  withhtml = True
        # /test?with-html&without-html                  withhtml = False
        # /test?with-html=false&without-html=false      withthml = True
        
        # ######################################################################
        # withdata

        # default False, case insensitive
        # /test                                         withdata = False
        # /test?withdata                                withdata = True
        # /test?withdata=True                           withdata = True
        # /test?withdata=true                           withdata = True
        # /test?withdata=trUE                           withdata = True
        
        # everything else is False
        # /test?withdata=False                          withdata = False
        # /test?withdata=false                          withdata = False
        # /test?withdata=FaLSe                          withdata = False
        # /test?withdata=nigges                         withdata = False
        
        # default False, case insensitive
        # /test?without-data                            withdata = False
        # /test?without-data=True                       withdata = False
        # /test?without-data=true                       withdata = False
        # /test?without-data=tRUe                       withdata = False
        
        # everything else is True
        # /test?without-data=False                      withdata = True
        # /test?without-data=false                      withdata = True
        # /test?without-data=faLSE                      withdata = True
        # /test?without-data=yeah                       withdata = True
        
        # order matters, last one wins
        # /test?without-data&with-data                  withdata = True
        # /test?with-data&without-data                  withdata = False
        # /test?with-data=false&without-data=false      withthml = True
        
        # ######################################################################

        testblocks = {
            'withhtml': {
                'Test 1: default True, case insensitive': [
                    { 'url': '/test'                                       , 'value': True  },
                    { 'url': '/test?with-html'                             , 'value': True  },
                    { 'url': '/test?with-html=True'                        , 'value': True  },
                    { 'url': '/test?with-html=true'                        , 'value': True  },
                    { 'url': '/test?with-html=trUE'                        , 'value': True  }
                ],
                'Test 2: everything else is False': [
                    { 'url': '/test?with-html=False'                       , 'value': False },
                    { 'url': '/test?with-html=false'                       , 'value': False },
                    { 'url': '/test?with-html=FaLSe'                       , 'value': False },
                    { 'url': '/test?with-html=nigges'                      , 'value': False }
                ],
                'Test 3: default False, case insensitive': [
                    { 'url': '/test?without-html'                         , 'value': False },
                    { 'url': '/test?without-html=True'                    , 'value': False },
                    { 'url': '/test?without-html=true'                    , 'value': False },
                    { 'url': '/test?without-html=tRUe'                    , 'value': False }
                ],
                'Test 4: everything else is True': [
                    { 'url': '/test?without-html=False'                   , 'value': True  },
                    { 'url': '/test?without-html=false'                   , 'value': True  },
                    { 'url': '/test?without-html=faLSE'                   , 'value': True  },
                    { 'url': '/test?without-html=yeah'                    , 'value': True  }
                ],
                'Test 5: order matters, last one wins': [
                    { 'url': '/test?without-html&with-html'               , 'value': True  },
                    { 'url': '/test?with-html&without-html'               , 'value': False },
                    { 'url': '/test?with-html=false&without-html=false'   , 'value': True  }
                ]
            },
            'withdata': {
                'Test 1: default True, case insensitive': [
                    { 'url': '/test'                                       , 'value': False },
                    { 'url': '/test?with-data'                             , 'value': True  },
                    { 'url': '/test?with-data=True'                        , 'value': True  },
                    { 'url': '/test?with-data=true'                        , 'value': True  },
                    { 'url': '/test?with-data=trUE'                        , 'value': True  }
                ],
                'Test 2: everything else is False': [
                    { 'url': '/test?with-data=False'                       , 'value': False },
                    { 'url': '/test?with-data=false'                       , 'value': False },
                    { 'url': '/test?with-data=FaLSe'                       , 'value': False },
                    { 'url': '/test?with-data=nigges'                      , 'value': False }
                ],
                'Test 3: default False, case insensitive': [
                    { 'url': '/test?without-data'                         , 'value': False },
                    { 'url': '/test?without-data=True'                    , 'value': False },
                    { 'url': '/test?without-data=true'                    , 'value': False },
                    { 'url': '/test?without-data=tRUe'                    , 'value': False }
                ],
                'Test 4: everything else is True': [
                    { 'url': '/test?without-data=False'                   , 'value': True  },
                    { 'url': '/test?without-data=false'                   , 'value': True  },
                    { 'url': '/test?without-data=faLSE'                   , 'value': True  },
                    { 'url': '/test?without-data=yeah'                    , 'value': True  }
                ],
                'Test 5: order matters, last one wins': [
                    { 'url': '/test?without-data&with-data'               , 'value': True  },
                    { 'url': '/test?with-data&without-data'               , 'value': False },
                    { 'url': '/test?with-data=false&without-data=false'   , 'value': True  }
                ]
            }
        }

        for testblock_key in testblocks:
            testblock = testblocks[testblock_key]
            
            for testcases_key in testblock:
                testcases = testblock[testcases_key]
                
                for testcase in testcases:
                    instance = BaseHandler()
                    instance.request = webapp2.Request.blank(testcase['url'])
                    instance.extract_parameter()
                    attr = getattr(instance, testblock_key)
                    self.assertEqual(testcase['value'], attr, testblock_key + ': ' + str(testcase['url']) + ' should evalute to ' + str(testcase['value']) + ', but was ' + str(attr));

if __name__ == '__main__':
    unittest.main()
